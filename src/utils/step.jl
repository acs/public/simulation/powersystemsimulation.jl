@inline function Step(condition::Bool, @nospecialize(trueval), @nospecialize(falseval))
    return ifelse(condition, trueval, falseval)
end

ModelingToolkit.@register Step(x, trueval, falseval)
#@eval ModelingToolkit using ..Main: if_else