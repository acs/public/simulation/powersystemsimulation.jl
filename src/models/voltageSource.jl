function ConstantVoltage(;name, V = 1.0)
    @named port = Port()
    @unpack v = port
    @parameters V=V
    eqs = [
           v ~ V
          ]
    extend(ODESystem(eqs, t, [], [V]; name=name), port)
end

function StepVoltage(;name, step, V1 = 1.0, V2 = 2.0)
    @named port = Port()
    @unpack v = port
    
    v_step(i) = Step(i > step,V2,V1)
    @variables _t
    v_step(_t)

    @parameters t
    eqs = [
            v ~ v_step(t)
    ]
    extend(ODESystem(eqs, t, [], []; name=name), port)

end