function Inductor(; name, L = 1.0)

    @named port = Port()
    @unpack v,i = port
    @parameters L=L
    D = Differential(t)
    eqs = [
           D(i) ~ v / L
          ]
    extend(ODESystem(eqs, t, [], [L]; name=name), port)
end