@parameters t
function Pin(;name)
    @variables v(t) i(t)
    ODESystem(Equation[], t, [v, i], [], name=name, defaults=[v=>1.0, i=>1.0])
end

function Port(;name)
    @named p = Pin()
    @named n = Pin()

    @variables v(t)=1.0 i(t)=1.0
    eqs = [
            v ~ p.v - n.v
            0 ~ p.i + n.i
            i ~ p.i
        ]
    compose(ODESystem(eqs, t, [v, i], []; name=name), p, n)
end

function connect_pins(ps...)
    eqs = [
        0 ~ sum(p->p.i, ps) # KCL
          ]
    # KVL
    for i in 1:length(ps)-1
        push!(eqs, ps[i].v ~ ps[i+1].v)
    end

    return eqs
end