@parameters t

function Resistor(;name, R = 1.0)
    @named port = Port()
    @unpack v, i = port
    @parameters R=R
    eqs = [
           v ~ i * R
          ]
    extend(ODESystem(eqs, t, [], [R]; name=name),port)
end

function StepLoad(;name, step = 1.0, R1 = 1.0, R2 = 2.0)
    r_step(i) = Step(i > step,R2,R1)
    @variables _t
    r_step(_t)
    
    @named port = Port()
    @unpack v, i = port

    eqs = [
           v ~ i * r_step(t)
          ]
    extend(ODESystem(eqs, t, [], []; name=name),port)
end