function Capacitor(;name, C = 1.0)

    @named port = Port()
    @unpack v,i = port
    @parameters C=C
    D = Differential(t)
    eqs = [
           D(v) ~ i / C
          ]
    extend(ODESystem(eqs, t, [], [C]; name=name), port)
end