using PowerSystemSimulation
using Plots

R = 1.0
L = 1.0
V = 1.0
@named resistor = Resistor(R=R)
@named inductor = Inductor(L=L)
@named source = ConstantVoltage(V=V)
@named ground = Ground()

rl_eqs = [
          connect_pins(source.p, resistor.p)
          connect_pins(resistor.n, inductor.p)
          connect_pins(inductor.n, source.n, ground.g)
        ]

@parameters t
@named rl_model = compose(ODESystem(rl_eqs, t; name=:_rl_models), resistor, inductor, source, ground)

sys = structural_simplify(rl_model)
states(sys)
u0 = [
        inductor.i => 0.0
    ]
prob = ODAEProblem(sys, u0, (0, 10.0))
@time sol = solve(prob)
plot(sol)