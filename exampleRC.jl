using PowerSystemSimulation
using Plots

R = 1.0
C = 1.0
V = 1.0
@named resistor = Resistor(R=R)
@named capacitor = Capacitor(C=C)
@named source = ConstantVoltage(V=V)
@named ground = Ground()

rc_eqs = [
          connect_pins(source.p, resistor.p)
          connect_pins(resistor.n, capacitor.p)
          connect_pins(capacitor.n, source.n, ground.g)
        ]

@parameters t
@named rc_model = compose(ODESystem(rc_eqs, t; name=:_rc_models), [resistor, capacitor, source, ground])

sys = structural_simplify(rc_model)
u0 = [
        capacitor.v => 0.0
    ]
prob = ODAEProblem(sys, u0, (0, 10.0))
@time sol = solve(prob)
plot(sol)