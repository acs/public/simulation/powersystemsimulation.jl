# PowerSystemSimulation.jl

## Install
1. Clone repository. Recommended: Julia provides a directory meant to include currently developed packages:
-   Linux: `/home/user/.julia/dev`
-   Windows: `USER/.julia/dev`

```
cd path/as/above
git clone git@git.rwth-aachen.de:acs/public/simulation/powersystemsimulation.jl.git PowerSystemSimulation
```

2. Make package available for development
    - Open Julia REPL `julia` and go into Package mode (press `]` in REPL)
    - `pkg> dev path/to/package`
3. Activate package environment to keep track of dependencies etc.
    - `pkg> activate path/to/package`

4. Execute example, e.g. with `include("example.jl")` from REPL
    