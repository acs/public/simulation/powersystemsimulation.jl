using PowerSystemSimulation
using Plots

R1 = 1.0
R2 = 5.0
C = 1.0
V1 = 1.0
V2 = 10.0

@named stepLoad = StepLoad(step=1.0,R1=R1,R2=R2)
@named capacitor = Capacitor(C=C)
@named stepSource = StepVoltage(step=1.0,V1=V1,V2=V2)
@named ground = Ground()

rc_eqs = [
          connect_pins(stepSource.p, stepLoad.p)
          connect_pins(stepLoad.n, capacitor.p)
          connect_pins(capacitor.n, stepSource.n, ground.g)
        ]

@parameters t
@named rc_model = compose(ODESystem(rc_eqs, t; name=:_rc_models), [stepLoad, capacitor, stepSource, ground])

sys = structural_simplify(rc_model)
u0 = [
        capacitor.v => 0.0
    ]
prob = ODAEProblem(sys, u0, (0, 10.0))
@time sol = solve(prob)
plot(sol)